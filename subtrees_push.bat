@echo off

PUSHD %~dp0

SET GIT=git.exe
IF EXIST      "%ProgramFiles%\Git\bin\git.exe" SET      GIT=%ProgramFiles%\Git\bin\git.exe
IF EXIST "%ProgramFiles(x86)%\Git\bin\git.exe" SET GIT=%ProgramFiles(x86)%\Git\bin\git.exe

SET PLINK=plink.exe
IF EXIST      "%ProgramFiles%\PuTTY\plink.exe" SET      PLINK=%ProgramFiles%\PuTTY\plink.exe
IF EXIST "%ProgramFiles(x86)%\PuTTY\plink.exe" SET PLINK=%ProgramFiles(x86)%\PuTTY\plink.exe

Set GIT_SSH=%PLINK%

"%GIT%" subtree push --prefix tools/NuGet tools_NuGet master

POPD
pause
