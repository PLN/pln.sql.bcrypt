@echo off

PUSHD %~dp0

SET GIT=git.exe
IF EXIST      "%ProgramFiles%\Git\bin\git.exe" SET      GIT=%ProgramFiles%\Git\bin\git.exe
IF EXIST "%ProgramFiles(x86)%\Git\bin\git.exe" SET GIT=%ProgramFiles(x86)%\Git\bin\git.exe

SET PLINK=plink.exe
IF EXIST      "%ProgramFiles%\PuTTY\plink.exe" SET      PLINK=%ProgramFiles%\PuTTY\plink.exe
IF EXIST "%ProgramFiles(x86)%\PuTTY\plink.exe" SET PLINK=%ProgramFiles(x86)%\PuTTY\plink.exe

Set GIT_SSH=%PLINK%

"%GIT%" fetch tools_NuGet master
"%GIT%" subtree pull --prefix tools/NuGet tools_NuGet master --squash

POPD
pause
